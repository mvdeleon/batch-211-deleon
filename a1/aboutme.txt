I grew up in Zamboanga City and moved here in Luzon when I need to find work. We do not have that much of opportunities in the province.

I worked in BPO for around 12 years. That was my first job.

I might have already worked for more than 15 different companies. I am a self confessed call center hopper. Not that I can't do it but I easily get bored.

I was given the opportunity to work as a freelancer in Upwork and so far, earning much higher than I was getting from BPO.

That leads me to revisit my urge to upskill in web development and coding.

I searched for potential schools but I did not find that fits my time. I am working night time.

Then I found Zuitt. The rest is history.